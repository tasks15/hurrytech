import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-syllabus',
  templateUrl: './syllabus.component.html',
  styleUrls: ['./syllabus.component.css']
})
export class SyllabusComponent implements OnInit {

  constructor() { }

  tid =1;
  stid =1;

  ngOnInit(): void {
  }

  topics = [
    { id:1,
      topic_name:"testing",
      tpoic_des:"des",
      time:"dsd",
      sub_topic:[
        {sid:1,name:"sub",des:"dsdf"}
      ]
    }
  ]


  addmain_topic(){
    this.tid++;
    this.stid ++;
    this.topics.push({ id:this.tid,topic_name:"Topic",tpoic_des:"",time:"",sub_topic:[{sid:this.stid,name:"",des:""}
      ]
    });
    this.topics = this.topics
    console.log(this.topics)
  }
  add_subtopic(tid:any){
      this.stid++;
    this.topics.forEach(element => {
        if(element.id == tid){

            element.sub_topic.push({sid:this.stid,name:"",des:""})
        }
        
      });
  }

  remove_subtopic(stid:any, tid:any){
    console.log(stid,tid)
      this.topics.forEach(element => {
        if(element.id == tid){

           element.sub_topic.forEach((element1,index,object) => {

             if(element1.sid== stid)
                console.log(element1,index)
                 object.splice(index,1)
             
           });
        }
        
      });
  }
}
