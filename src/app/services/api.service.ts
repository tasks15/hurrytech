import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor() { }

  public image :any = "../../../assets/img/timothy-paul-smith-256424-1200x800.jpg"
}
