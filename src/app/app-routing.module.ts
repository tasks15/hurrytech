import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './components/account/account.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LayoutComponent } from './components/layout/layout.component';
import { LoginComponent } from './components/login/login.component';
import { SyllabusComponent } from './components/syllabus/syllabus.component';
import { Page1Component } from './task2components/page1/page1.component';
import { Page2Component } from './task2components/page2/page2.component';
import { Page3Component } from './task2components/page3/page3.component';

const routes: Routes = [
  {path:'login',component:LoginComponent},

  {
    path:'',
    component:LayoutComponent,
    children:[
      {path:'',component:DashboardComponent},
      {path:'syllabus',component:SyllabusComponent},
      {path:'dashboard',component:DashboardComponent},
      {path:'account',component:AccountComponent},
      {path:'video',component:Page1Component},
      {path:'pdf',component:Page2Component},
      {path:'quiz',component:Page3Component},


    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
