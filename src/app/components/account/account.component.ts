import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  editable = true;

  constructor(public api:ApiService) { }

  ngOnInit(): void {
  }
  OnEdit(){
    this.editable = !this.editable
  }

  image_select(event: any) {
    
    console.log("asd")
  
    if (event.target.files && event.target.files[0]) {
      const file =  event.target.files[0];

      var reader = new FileReader();
  
      reader.onload = (event: ProgressEvent) => {
        this.api.image =reader.result;
      }
      // this.api.image = (<FileReader>event.target).result;

      reader.readAsDataURL(file);
    }

    console.log(this.api.image)
  }

}
