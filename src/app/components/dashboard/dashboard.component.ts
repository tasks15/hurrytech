import { Component, OnInit,ViewChild ,AfterViewInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

export interface UserData {
  School_Name: string;
  Board: string;
  Medium: string;
  Class: string;
  id:number
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
 id=1;

  displayedColumns: string[] = ['select','School_Name', 'Board', 'Medium', 'Class'];
  dataSource: MatTableDataSource<UserData>;
  
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

    table_data =[
      {School_Name:"qe",Board:"sadsad",Medium:"Medium",Class:"asd",id:this.id},
    ]
    to_delete_items :any=[];
  constructor() { 
     // Create 100 users

     // Assign the data to the data source for the table to render
     this.dataSource = new MatTableDataSource(this.table_data);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  addrow(){
    this.id++;
    this.table_data.push( {School_Name:"",Board:"",Medium:"",Class:"",id:this.id});
    this.dataSource = new MatTableDataSource(this.table_data);  
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onCheck(boxid: any){
    
      var item_found =false;
    this.to_delete_items.forEach((element: any,index: any,object: any) => {

      if(element == boxid){
        object.splice(index,1);
        item_found = true;
      }
      
    });

    if(item_found ==false){
     
        this.to_delete_items.push(boxid);
      
    }

   console.log(this.to_delete_items)
  }

  delete(){

    this.to_delete_items.forEach((element: any) => {

        this.table_data.forEach((element1: any,index, object) => {
         
          if(element1.id == element){
            object.splice(index, 1);
            console.log(element)
          }
          
        });

    });
    console.log(this.table_data   )
    this.dataSource = new MatTableDataSource(this.table_data);  
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}

